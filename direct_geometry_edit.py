home_path = QgsProject.instance().homePath()
layer = iface.activeLayer()
feature = layer.getFeature(5)
if feature.isValid():
    geometry = QgsGeometry.fromPointXY(QgsPointXY(-10236737,  13653691))
    geometry_map = {feature.id(): geometry}
    layer.dataProvider().changeGeometryValues(geometry_map)
    layer.triggerRepaint()