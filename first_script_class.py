"""FirstScript: A simple class used to load a layer  in QGIS and change its color."""

from PyQt5.QtGui import QColor
from PyQt5.QtCore import Qt


class FirstScript:
    """Class to load and render the world_borders shapefile."""

    def __init__(self, iface):
        self.iface = iface

    def load_layer(self):
        """Load the world_borders shapefile and add it to the map."""
        self.iface.addVectorLayer('C:/Users/Eron Lloyd/Projects/pyqgis_practice/pyqgis3_files/data/world_borders.shp',
                                  'world_border', 'ogr')

    def change_color(self):
        """Change the color of the active layer and update the legend."""
        active_layer = self.iface.activeLayer()
        renderer = active_layer.renderer()
        symbol = renderer.symbol()
        symbol.setColor(QColor(Qt.red))
        active_layer.triggerRepaint()
        self.iface.layerTreeView().refreshLayerSymbology(active_layer.id())

    def open_attribute_table(self):
        """Open the attribute table for the active layer."""
        self.iface.showAttributeTable(self.iface.activeLayer())


def run_script(iface):
    """Run the script by instantiating FirstScript and calling methods."""
    print("Creating object.")
    fs = FirstScript(iface)
    print("Loading layer.")
    fs.load_layer()
    print("Changing color.")
    fs.change_color()
    print("Opening attribute table.")
    fs.open_attribute_table()
