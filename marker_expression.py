sym = QgsMarkerSymbol.createSimple({
    'name': 'circle',
    'color': 'blue',
    'size_expression': 'marker_size',
    'outline_width': '1'
    })

layer = iface.activeLayer()
renderer = layer.renderer()
renderer.setSymbol(sym)
layer.triggerRepaint()
iface.layerTreeView().refreshLayerSymbology(layer.id())