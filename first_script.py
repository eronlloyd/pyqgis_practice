from PyQt5.QtGui import QColor
from PyQt5.QtCore import Qt
from qgis.core import QgsVectorLayer, QgsProject
from qgis.utils import iface


def load_layer():
    layer = QgsVectorLayer('C:/Users/Eron Lloyd/Projects/pyqgis_practice/pyqgis3_files/data/world_border.shp',
        'world_border', 'ogr')
    if layer.isValid():
        print("Layer is valid.")
        QgsProject.instance().addMapLayer(layer)
    else:
        print("Layer is invalid.")


def change_color():
    active_layer = iface.activeLayer()
    renderer = active_layer.renderer()
    symbol = renderer.symbol()
    symbol.setColor(QColor(Qt.red))
    active_layer.triggerRepaint()
    iface.layerTreeView().refreshLayerSymbology(active_layer.id())


def open_attribute_table():
    iface.showAttributeTable(iface.activeLayer())

load_layer()