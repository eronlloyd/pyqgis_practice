sym = QgsLineSymbol.createSimple({
    'color': 'red',
    'customdash': '8;4',
    'use_custom_dash': '1',
    'width': '2'
    })

layer = iface.activeLayer()
renderer = layer.renderer()
renderer.setSymbol(sym)
layer.triggerRepaint()
iface.layerTreeView().refreshLayerSymbology(layer.id())