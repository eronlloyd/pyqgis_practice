import os
import sys

from PyQt5.QtGui import QColor

from qgis.utils import iface
from qgis.core import *

from osgeo import ogr
from osgeo import gdal

sys.path.append(os.path.dirname(os.path.realpath(__file__)))


def addLayer(uri, name=None):
    """ Generic attempt to add a layer by attempting to
        open it in various ways"""
    # try to open using ogr
    lyr = ogr.Open(uri)
    if lyr:
        return addOgrLayer(uri, name)
    else:
        # try to open using gdal
        lyr = gdal.Open(uri)
        if lyr:
            return addGdalLayer(uri, name)
        else:
            return None


def addOgrLayer(layerpath, name=None):
    """ Add an OGR layer and return a reference to it.
        If name is not passed, the filename will be used
        in the legend.

        User should check to see if layer is valid before
        using it."""
    if not name:
        (path, filename) = os.path.split(layerpath)
        name = filename

    return iface.addVectorLayer(layerpath, name, 'ogr')


def addGdalLayer(layerpath, name=None):
    """Add a GDAL layer and return a reference to it"""
    if not name:
        (path, filename) = os.path.split(layerpath)
        name = filename

    return iface.addRasterLayer(layerpath, name)


def removeLayer(layer):
    QgsProject.instance().removeMapLayer(layer.id())
    iface.mapCanvas().refresh()


def createRGBA(color):
    (red, green, blue, alpha) = color.split(',')
    return QColor.fromRgb(int(red), int(green), int(blue), int(alpha))


def changeColor(layer, color):
    """ Change the color of a layer using
        Qt named colors, RGBA, or hex notation."""
    if ',' in color:
        # assume rgba color
        color = createRGBA(color)
        transparency = color.alpha() / 255.0
    else:
        color = QColor(color)
        transparency = None

    renderer = layer.renderer()
    symb = renderer.symbol()
    symb.setColor(color)
    if transparency:
        symb.setOpacity(transparency)
    layer.triggerRepaint()
    iface.layerTreeView().refreshLayerSymbology(layer.id())
