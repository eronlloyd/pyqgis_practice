class Point:
    """ Class to model a point in 2D space."""

    """ Size of our marker in pixels """
    marker_size = 4

    def __init__(self, x, y):
        """ Initialize the new Point object at x, y"""
        self.x = x
        self.y = y

    def draw(self):
        """Draw the point on the map canvas"""
        print("Drawing the point at {}, {}".format(self.x, self.y))

    def move(self, new_x, new_y):
        """ Move the point to a new location on the
            map canvas"""
        print("Moving the point to {}, {}".format(new_x, new_y))
        self.x = new_x
        self.y = new_y
