layer = iface.activeLayer()
feature = layer.getFeature(1)
if feature.isValid():
    geometry = QgsGeometry.fromPointXY(QgsPointXY(10, 10))
    geometry_map = {feature.id(): geometry}
    layer.dataProvider().changeGeometryValues(geometry_map)
    layer.triggerRepaint()
