layer = iface.activeLayer()
layer.startEditing()
layer.beginEditCommand('Edit')
layer.changeAttributeValue(1, layer.dataProvider().fieldNameIndex('name'),
                           'No Name, Jr.') 
layer.changeAttributeValue(1, layer.dataProvider().fieldNameIndex('city'),
                           'Tacoma, WA')
layer.endEditCommand()
layer.commitChanges()
