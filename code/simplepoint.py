class Point:

    marker_size = 4

    def __init__(self, x, y):
        self.x = x
        self.y = y

    def draw(self):
        print("Drawing the point at {}, {}".format(self.x, self.y))

    def move(self, new_x, new_y):
        print("Moving the point to {}, {}".format(new_x, new_y))
        self.x = new_x
        self.y = new_y
