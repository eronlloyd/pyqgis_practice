import os

from PyQt5.QtWidgets import (QFrame, QGridLayout, QMainWindow, QAction,
                             QActionGroup, QMessageBox)
from PyQt5.QtGui import QIcon
from qgis.gui import QgsMapCanvas, QgsMapToolZoom
from qgis.core import QgsProject, QgsVectorLayer

from map_tool import ConnectTool

import resources


class OurMainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)

        self.setupGui()
        self.project = QgsProject()

        self.add_ogr_layer('/data/pyqgis_data/alaska.shp')
        self.map_canvas.zoomToFullExtent()

    def setupGui(self):
        frame = QFrame(self)
        self.setCentralWidget(frame)
        self.grid_layout = QGridLayout(frame)

        self.map_canvas = QgsMapCanvas()
        self.grid_layout.addWidget(self.map_canvas)

        # setup action(s)
        self.zoomin_action = QAction(
            QIcon(":/ourapp/zoomin_icon"),
            "Zoom In",
            self)
        self.zoomin_action.setCheckable(True)

        self.connect_action = QAction(
            QIcon(":/ourapp/connect_icon"),
            "Connect",
            self)
        self.connect_action.setCheckable(True)

        # create toolbar
        self.toolbar = self.addToolBar("Map Tools")
        self.toolbar.addAction(self.zoomin_action)
        self.toolbar.addAction(self.connect_action)

        # connect the actions
        self.zoomin_action.triggered.connect(self.zoom_in)
        self.connect_action.triggered.connect(self.connect_pt)

        # create the map tool(s)
        self.tool_zoomin = QgsMapToolZoom(self.map_canvas, False)
        self.tool_connect = ConnectTool(self.map_canvas)
        self.tool_connect.line_complete.connect(self.connect_complete)

        # make tools checkable
        tool_group = QActionGroup(self)
        tool_group.addAction(self.zoomin_action)
        tool_group.addAction(self.connect_action)

    def add_ogr_layer(self, path):
        (name, ext) = os.path.basename(path).split('.')
        layer = QgsVectorLayer(path, name, 'ogr')
        self.project.addMapLayer(layer)
        self.map_canvas.setLayers([layer])

    def zoom_in(self):
        self.map_canvas.setMapTool(self.tool_zoomin)
        self.zoomin_action.setChecked(True)

    def connect_pt(self):
        self.map_canvas.setMapTool(self.tool_connect)
        self.connect_action.setChecked(True)

    def connect_complete(self, pt1, pt2):
        # create the line from the points
        QMessageBox.information(None,
                                "Connect Tool",
                                "Creating line from %s to %s"
                                % (pt1.toString(), pt2.toString()))
