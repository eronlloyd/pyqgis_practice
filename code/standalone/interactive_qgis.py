from PyQt5.QtWidgets import QApplication, QFrame, QGridLayout, QMainWindow
from qgis.gui import QgsMapCanvas
from qgis.core import QgsApplication, QgsProject, QgsVectorLayer

app = QApplication([])
QgsApplication.setPrefixPath("/Users/gsherman/apps/QGIS.app/Contents/MacOS", True)
QgsApplication.initQgis()

main_win = QMainWindow()
frame = QFrame(main_win)
main_win.setCentralWidget(frame)
grid_layout = QGridLayout(frame)

map_canvas = QgsMapCanvas()
grid_layout.addWidget(map_canvas)

layer = QgsVectorLayer(
    '/data/pyqgis_data/alaska.shp',
    'alaska',
    'ogr')

prj = QgsProject()
prj.addMapLayer(layer)

map_canvas.setLayers([layer])
map_canvas.zoomToFullExtent()

main_win.show()

# Need the following statement if running as a script
app.exec_()
