from PyQt5.QtGui import QColor
from PyQt5.QtCore import Qt
from qgis.core import QgsVectorLayer
from qgis.utils import iface

wb = iface.addVectorLayer('/data/pyqgis_data/world_borders.shp',
                          'world_borders', 'ogr')

active_layer = iface.activeLayer()
renderer = active_layer.renderer()
symbol = renderer.symbol()
symbol.setColor(QColor(Qt.red))
active_layer.triggerRepaint()

iface.layerTreeView().refreshLayerSymbology(active_layer.id())


iface.showAttributeTable(iface.activeLayer())
