"""Use the database provider to connect to PostgreSQL"""

# Import everything we might need
from qgis.core import QgsDataSourceUri
from qgis.utils import iface
import psycopg2


layer = iface.activeLayer()
provider = layer.dataProvider()
if provider.name() == 'postgres':
    uri = QgsDataSourceUri(provider.dataSourceUri())
    print(uri.uri())
    # create a connection using psycopg2
    db = psycopg2.connect(uri.connectionInfo())
    # execute a simple query
    cur = db.cursor()
    cur.execute("""select name from qgis_sample.airports
            order by name""")
    rows = cur.fetchall()
    for row in rows:
            print(row[0])
else:
    print("The selected layer %s is not a PostgreSQL layer" % layer.name())
    print("Unable to create a database connection")
