s = "QGIS loves Python"
# split on whitespace
s.split()
['QGIS', 'loves', 'Python']
# split into variables
(a, b, c) = s.split()
print(a, b, c)
# slice
s[0:1]
s[-1:]
s[:-1]
