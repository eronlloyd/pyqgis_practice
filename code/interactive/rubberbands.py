# get the world borders layer (the active layer)
wb = iface.activeLayer()
# get the feature for Iceland
features = wb.getFeatures(QgsFeatureRequest(QgsExpression("cntry_name='Iceland'")))
iceland = QgsFeature()
features.nextFeature(iceland)
# create the rubberband
rb = QgsRubberBand(iface.mapCanvas())
# set the fill color to red with 50% transparency
rb.setColor(QColor(255, 0, 0, 128))
# set the rubberband shape (geometry) to that of Iceland
rb.setToGeometry(iceland.geometry(), wb)
