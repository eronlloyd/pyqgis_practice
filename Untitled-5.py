request = QgsFeatureRequest().setFilterExpression( u'"id" in (1, 2, 3)')
layer = iface.activeLayer()
iter = layer.getFeatures(request)
feat = QgsFeature()
iter.nextFeature(feat)
box = feat.geometry().boundingBox()
while iter.nextFeature(feat):
    box.combineExtentWith(feat.geometry().boundingBox())
print(box)