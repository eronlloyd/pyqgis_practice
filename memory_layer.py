from qgis.core import QgsVectorLayer, QgsProject, QgsPoint, QgsFeature, QgsGeometry

print("Creating layer.")
mem_layer = QgsVectorLayer("LineString?crs=epsg:4326&field=id:integer&field=road_name:string&index=yes",
    "Roads", "memory")
QgsProject.instance().addMapLayer(mem_layer)

mem_layer.startEditing()
points = [QgsPoint(-150, 61), QgsPoint(-151, 62)]
feature = QgsFeature()
feature.setGeometry(QgsGeometry.fromPolyline(points))
feature.setAttributes([1, 'QGIS Lane'])
mem_layer.addFeature(feature)
mem_layer.commitChanges()
