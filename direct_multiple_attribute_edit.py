home_path = QgsProject.instance().homePath()
layer = iface.activeLayer()
provider = layer.dataProvider()
feature = layer.getFeature(4)
feature['name'] = 'TEST4'
feature['type'] = 'CURVED'
field_map = provider.fieldNameMap()
attrs = {field_map[key]: feature[key] for key in field_map}
print(attrs)
new_name = {feature.fieldNameIndex('name'): 'Test4'}
layer.dataProvider().changeAttributeValues({feature.id(): attrs})