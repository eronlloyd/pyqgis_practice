sym = QgsMarkerSymbol.createSimple({
    'name': 'square',
    'color': 'red',
    'size': '4',
    'outline_width': '1.5'
    })

layer = iface.activeLayer()
renderer = layer.renderer()
renderer.setSymbol(sym)
layer.triggerRepaint()
iface.layerTreeView().refreshLayerSymbology(layer.id())