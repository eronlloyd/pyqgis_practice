home_path = QgsProject.instance().homePath()
layer = iface.activeLayer()
layer.startEditing()
layer.beginEditCommand('Edit')
layer.changeAttributeValue(4, layer.dataProvider().fieldNameIndex('name'),
    'POINT 4')
layer.changeAttributeValue(4, layer.dataProvider().fieldNameIndex('marker_size'),
    10)
layer.endEditCommand()
layer.commitChanges()