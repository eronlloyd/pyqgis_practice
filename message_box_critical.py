# Pops up a critical message box
response = QMessageBox.critical(None,
    'Unsave',
    'You have unsaved changes',
    QMessageBox.Discard | QMessageBox.Save)
print(response)