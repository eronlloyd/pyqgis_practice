sym = QgsFillSymbol.createSimple({
    'style': 'diagonal_x',
    'color': 'blue',
    })

layer = iface.activeLayer()
renderer = layer.renderer()
renderer.setSymbol(sym)
layer.triggerRepaint()
iface.layerTreeView().refreshLayerSymbology(layer.id())