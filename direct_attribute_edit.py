home_path = QgsProject.instance().homePath()
layer = iface.activeLayer()
feature = layer.getFeature(4)
new_name = {feature.fieldNameIndex('name'): 'Test4'}
layer.dataProvider().changeAttributeValues({feature.id(): new_name})